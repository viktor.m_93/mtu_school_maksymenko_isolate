export const LocalConfig = {
    baseUrl: "http://petstore:8080/",
    mongo: {
        connectionUrl: "mongodb://mongo:27017/defaultDB",
    },
};