FROM alpine:latest

ARG GIT_USER
ARG GIT_TOKEN

RUN apk add git nodejs npm && \
    git clone https://$GIT_USER:$GIT_TOKEN@gitlab.com/viktor.m_93/mtu_school_-maksymenko.git && \
    cd mtu_school_-maksymenko && \
    npm install

COPY /srs/local.config.ts ./mtu_school_-maksymenko/config/

WORKDIR /mtu_school_-maksymenko

ENTRYPOINT ["tail", "-f", "/dev/null"]
